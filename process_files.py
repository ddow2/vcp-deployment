import re  # import the regular expressions module
import shutil
import os
from PythonLogger import get_logger

# Gather information from the VAT Runner Properties file (e.g. vatrunner.properties) file to build the Ansible hosts files
# and Ansible vars file.
hosts_file = "./ansible/hosts"


def build_hosts_file(vat_file_name):
    logger = get_logger()
    logger.info("Start - using: '" + vat_file_name + "' file.")

    if os.path.exists(hosts_file):
        shutil.copy(hosts_file, hosts_file + "_orig")
        os.remove(hosts_file)

    # Make sure the files are present
    try:
        vat_file_handle = open(vat_file_name, "r")
        vatrunner_lines = vat_file_handle.readlines()
        hosts_file_handle = open(hosts_file, "a")
    except Exception as e1:
        logger.error(e1)
        exit(2)

    try:
        # Copy the Ansible block (START -> END) from the vatrunner.properties file to the hosts file
        extract_file_block(hosts_file_handle, vatrunner_lines, '^.*START.*Ansible.*', '^.*END.*Ansible.*', '', vat_file_name)
        # Then, extract the IP address from the mnode property line and add to the hosts file
        hosts_file_handle.write('[esxserversIP]')
        regex = '^vat.*esx\.host.*ip.*=.\s?(.*)'
        extract_info(hosts_file_handle, vatrunner_lines, regex, vat_file_name)
    except Exception as e2:
        logger.error(e2)
        hosts_file_handle.close()
        vat_file_handle.close()
        exit(2)

    try:
        # Now get the IP address from the mnode and add it to the hosts file
        hosts_file_handle.write('[mnodeIP]')
        regex = '^vat.*mnode.*ip.*=.\s?(.*)'
        extract_info(hosts_file_handle, vatrunner_lines, regex, vat_file_name)
    except Exception as e2:
        logger.error(e2)
        hosts_file_handle.close()
        vat_file_handle.close()
        exit(2)

    hosts_file_handle.close()
    vat_file_handle.close()
    logger.info("End - finished building the hosts file using the: '" + vat_file_name + "' file.")


# Gather information from the vatrunner.properties file to build the Ansible vars_file.yml files
def build_vars_file(vat_file_name):
    logger = get_logger()
    logger.info("Start - build the vars/vars_file.yml file.")

    vars_file_name = './vars/vars_file.yml'
    vars_file_orig_name = './vars/vars_file_orig.yml'

    # Make sure the files are present
    try:
        shutil.copyfile(vars_file_name, vars_file_orig_name)
        # open the files that are going to be worked with
        vat_file_handle = open(vat_file_name, "r")
        vars_file_handle = open(vars_file_name, "w+")
        vars_file_handle_orig = open(vars_file_orig_name, "r")
        # Read all of the lines from the vartunner.properties and vars_file_orig.yml file
        vatrunner_lines = vat_file_handle.readlines()
        vars_file_orig_lines = vars_file_handle_orig.readlines()
        vars_file_handle.write('---\n')
    except Exception as e1:
        shutil.copyfile(vars_file_orig_name, vars_file_name)
        vars_file_handle.close()
        vars_file_handle_orig.close()
        logger.error(e1)
        exit(2)

    # Copy the Ansible block from the vatrunner.properties file to the vars file
    try:
        extract_file_block(vars_file_handle, vatrunner_lines, '^.*START.*vars.*', '^.*END.*vars.*', '    ', vat_file_name)
        vat_file_handle.close()
        vars_file_handle.write('    ##### Below attributes shouldn\'t need to be modified\n')
        extract_file_block(vars_file_handle, vars_file_orig_lines, '^.*####.*', '\Z', '', vat_file_name)
    except Exception as e2:
        shutil.copyfile(vars_file_orig_name, vars_file_name)
        vars_file_handle.close()
        vars_file_handle_orig.close()
        logger.error(e2)
        exit(2)

    vars_file_handle.close()
    vars_file_handle_orig.close()
    logger.info("End - finished building the: '" + vars_file_name + "' file.")


# Extract a block from the vatrunner.properties file and insert into the specified output file.
def extract_file_block(output_file, input_file_lines, start_line, end_line, padding, vat_file_name):
    logger = get_logger()
    logger.info("Start - build the rest of the vars_file.yml file.")
    # Copy the lines between 'START - ...' and 'END - ...'
    pattern = re.compile(start_line)
    found_start = 'false'
    for line in input_file_lines:
        match = pattern.match(line)
        # Looks for the 'START ...' line
        if match and found_start == 'false':
            # Now that we found the start line, update the regex to search for the 'END..' line
            pattern = re.compile(end_line)
            found_start = 'true'
            continue
        elif found_start == 'true':
            # Break out as soon as we find the 'END..' line
            if match:
                break
            output_file.write(padding+line)
    if found_start == 'false':
        output_file.close()
        # restore the hosts file just in case something was in it before we opened it
        raise Exception("extract_file_block error: \'" + start_line + "\' not found in " + vat_file_name)
    output_file.write('\n')
    logger.info("End - finished building the rest of the vars_file.yml file.")


# Extract the IP address from the mnodes and add to the hosts file
def extract_info(hosts_file_handle, vatrunner_lines, regex, vat_file_name):
    logger = get_logger()
    logger.info("Extract '" + regex + "'from the '"+ vat_file_name +"' file.")

    pattern = re.compile(regex)
    found_mnode_ip = 'false'
    for line in vatrunner_lines:
        match = pattern.match(line)
        if match:
            found_mnode_ip = 'true'
            hosts_file_handle.write('\n' + match.group(1))
    if found_mnode_ip == 'false':
        raise Exception(" mnode IP addresses not found in \'" + vat_file_name + "\' file.")
    hosts_file_handle.write('\n\n')
    logger.info("End - finished building the Ansible/hosts file.")


# Extract the first occurrence of the regex and return it.
def extract_vcenter_info(vatrunner_lines, regex, vat_file_name):
    logger = get_logger()
    logger.info("Extract '" + regex + "'from the '"+ vat_file_name +"' file.")
    pattern = re.compile(regex)
    for line in vatrunner_lines:
        match = pattern.match(line)
        if match:
            return match.group(1)
