```
##############################################################################################################################################################
# This package will install the newest VCP plugin in a vCenter and run the VCP Validation Acceptance Tests.
#
# This package must be installed on a Jenkins Windows slave machine (i.e. 10.117.32.196) that has connectivity to your vCenter.
# The Windows machine must have Ubuntu installed on it in order to run Ansible (maybe this will change someday).
# The Python wrapper script - deploy_VCP_build.py - needs to be called from the Jenkins post-build step with the following parameters:
#   -i, ignore_warnings - Continue if encountering any Ansible warnings
#   -t, take_gold_snapshots - Before installing the VCP plugin, take a gold snapshot on every machine specified in /ansible/hosts file.
#       If the VM already has a snapshot named "gold", a new snapshot will not be taken
#   -v, vatrunner_properties - The user-populated vatrunner.properties file that has the information of the vCenters and VMs
#       that the tests will run against.
##############################################################################################################################################################
```
