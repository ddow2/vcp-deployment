import subprocess
from process_files import build_vars_file
from process_files import build_hosts_file
from process_files import extract_vcenter_info
import argparse
from PythonLogger import get_logger
import requests
import time

ignore_warnings = "ignore_warnings"
take_gold_snapshots = "take_gold_snapshots"
vcenter_version = "vcenter_version"
vatrunner_properties = "vatrunner_properties"

# This is the URL to the Jenkins server to get the latest VCP build
url_prefix = "http://hi-jenkins.den.solidfire.net:8080/job/VCP%20Release/lastSuccessfulBuild/"

#####################################################################################################################
# This is an Application that calls Ansible, Powershell, and MS DOS to run the VCP system testing.
#
# NOTE: This Application runs in an Ubuntu shell on a Windows Jankins Slave machine (i.e. 10.117.32.196). This Windows
#       machine must have the VCP VAT Test Suite installed.
#
# The user must perform the following before executing this application
#  1. Update the vatrunner.properties file on the Jenkins VCP slave machine to
#     point to the VMs that are going to be used in the test environment.
#  2. ** TEMPORARY ** Update the C:\Users\solidfire\VAT\powershell\ConnectHosts.ps1 script on the VCP slave machine
#     to include the ESX host that you'd like to reconnect to (along with your vCenter server IP).
#     Once the Ansible bug is fixed, this will no longer be necessary.
#  3. Update the VCP Jenkins job to call this Python script with the following options:
#     - Ignore Ansible warnings:  true or false
#     - Take a gold snapshot of all of the machines in the test environment:  true or false
#     - The name of the properties file that contains the test environment (e.g. vatrunner.properties)
#  Jenkins will call this app after a successful build.
#
# This app will:
#  1. Create a new ./ansible/hosts file and ./vars/vars_file.yml from the vatrunner.properties file (Python)
#  2. If the take_gold_snapshots argument is true, the code will take a gold snapshot if one doesn't exist (Ansible)
#  3. After the snapshots are restored, the ESX servers may need to be reconnected to the vCenter (this is only
#     necessary on vCenter 6.7).  This functionality only works with Ansible if the ESXi servers are in a cluster.
#     So for now, call an existing PowerShell script to reconnect the ESXi servers (PowerShell)
#  4. Connect to the mnode, install the VCP plugin, then register with the vCenter (Ansible)
#  5. For vCenter 6.7, a browser needs to be opened, administrator needs to log out, then log back in to properly
#     register the VCP plug-in (PowerShell, Selenium)
#  6. Open the Jenkins slave in a remote console, then launch the vatrunner.bat script (Windows DOS, Java, Sikuli, Selenium).
#
#####################################################################################################################

parser = argparse.ArgumentParser("")
parser.add_argument("-i", "--"+ignore_warnings, choices=["true", "false"], required=True, help="Continue if Ansible returns warnings")
parser.add_argument("-t", "--"+take_gold_snapshots, choices=["true", "false"], required=True, help="Take gold snapshots before installation")
parser.add_argument("-v", "--"+vatrunner_properties, required=True, help="Windows path to the vatrunner.properties file")
args = vars(parser.parse_args())

# Configure the logger
logger = get_logger()


# Log the args so we know what we ran
logger.info(args)

# Take gold snapshots with memory.
def take_snapshots():
    # This will only be done if the user specifies they want to take a snapshot using the take_gold_snapshots argument
    # If Ansible sees that there is already a snapshot with the name "gold", a new snapshot will not be created.
    logger.info("Start - Taking gold snapshots of the hosts specified in the Take Snapshot playbook.")

    # This step shouldn't be needed because the user should've taken a snapshot before running this script.
    if args[take_gold_snapshots] == 'true':
        try:
            sb = subprocess.check_output(
                "ansible-playbook -v yaml/takeSnapshotWithMemory.yml",
                stderr=subprocess.STDOUT,
                shell=True)

        except subprocess.CalledProcessError:
            # If Ansible fails for any reason, stop the run.
            logger.error("\nCould not take snapshots VMs. Please see vcp_ansible.log file for details.\n")
            exit(2)

        # There are some cases where we get warnings (e.g. invalid host in a YAML file). If so, exit if we care about warnings.
        if sb.__contains__("WARNING") and args[ignore_warnings] == 'false':
            logger.warning("\nAnsible WARNING encountered - execution stopped: \n"+sb)
            exit(1)
        logger.info("End - Completed taking gold snapshots.\n")
    else:
        logger.info("End - The 'take_gold_snapshots' flag was set to 'false', no snapshot taken, continuing.\n")


# This will revert all of the VMs in your /etc/ansible/hosts file under the [snapshotVMs] group to "gold".
# If the user selects the above option (to take gold snaphots), we will still revert the snapshots to gold just in case some of the VMs
# in the ansible/hosts list already had a gold snapshot.
def revert_snapshots_to_gold():
    logger.info("Start - revert the snapshots to gold.\n")
    try:
        sb = subprocess.check_output(
            "ansible-playbook -v yaml/revertSnapshot.yml",
            stderr=subprocess.STDOUT,
            shell=True)

    except subprocess.CalledProcessError:
        # If Ansible fails for any reason, stop the run.
        logger.error("\nCould not revert snapshot. Please see vcp_ansible.log file for details.\n")
        exit(2)

    # There are some cases where we get warnings. If so, exit if we care about warnings.
    if sb.__contains__("WARNING") and ignore_warnings == 'false':
        logger.warning("\n"+sb)
        exit(1)
    logger.info("End - revert the snapshots to gold.\n")

    # After reverting the snapshots, we need to wait about 10 minutes for everything to reinitialize
    time.sleep(600)


# The below code was invoked when there was a bug with the vmware_host.py module.
# Ubuntu location here: /usr/lib/python2.7/dist-packages/ansible/modules/cloud/vmware/vmware_host.py
# Before the fix, Ansible could only reconnect to an ESXi server only if it was under a cluster.
# Fix Info: https://github.com/ansible/ansible/issues/44772
#
# Leave code here just in case we need to make more Powershell calls from Windows Ubuntu.
#   logger.info("Start - Reconnect ESX hosts using the ConnectHosts PowerShell script.\n")
#   ps = subprocess.Popen(["powershell.exe","C:\\Users\\solidfire\\VAT\\powershell\\ConnectHosts.ps1 "+args[vcenter_version]+""], stdout=sys.stdout)
#   ps.communicate()
#
# Using Ansible, reconnect ESX servers to the vCenter
def reconnect_esx_servers():
    logger.info("Start - Reconnect the ESXi servers to the vCenter.\n")
    try:
        subprocess.check_output(
            "ansible-playbook -v yaml/reconnectVCenter.yml",
            stderr=subprocess.STDOUT,
            shell=True)
    except subprocess.CalledProcessError:
        # If Ansible fails for any reason, stop the run.
        logger.error("\nCould not connect ESX servers to vCenter. Please see vcp_ansible.log file for details.\n")
        exit(2)

    logger.info("End - ESXi servers have been reconnected to the vCenter.\n")


# Using Ansible, remove the old VCP package, then install the new one from Jenkins
def update_mnode():
    # TODO: this process eventually needs to be updated using ZIP files instead of Debian
    logger.info("Start - Update the mNode with the newest version of the VCP Plugin.")

    # Get the the latest VCP build.
    url = url_prefix + "api/json"
    json_string = requests.get(url).json()

    # NOTE: element 0 is the debian package and element 1 is the ZIP package (just change to 1 when working with the ZIP file)
    relativePath = json_string['artifacts'][0]['relativePath']
    artifact_url = url_prefix + "artifact/" + relativePath
    fileName = json_string['artifacts'][0]['fileName']
    logger.info("URL: " + artifact_url)
    logger.info("File Name: " + fileName)

    # Get the vCenter IP address and creds from the vatrunner.properties file
    vat_file_handle = open(args[vatrunner_properties], "r")
    # Read all of the lines from the vartunner.properties
    vatrunner_lines = vat_file_handle.readlines()

    # Get the vCenter IP Address
    regex = '^vat.*vcenter.1.*ip.*=.\s?(.*)'
    vcenter_ip = extract_vcenter_info(vatrunner_lines, regex, args[vatrunner_properties])

    # Get the vCenter user name
    regex = '^vat.*vcenter.1.*username.*=.\s?(.*)'
    vcenter_username = extract_vcenter_info(vatrunner_lines, regex, args[vatrunner_properties])

    # Get the vCenter password
    regex = '^vat.*vcenter.1.*password.*=.\s?(.*)'
    vcenter_password = extract_vcenter_info(vatrunner_lines, regex, args[vatrunner_properties])

    try:
        subprocess.check_output(
            'ansible-playbook -v yaml/updateMnode.yml -u admin --become --extra-vars ansible_become_pass=solidfire -e '"pkgUrl=" + artifact_url +
            " -e fName=" + fileName + " -e vcIP=" + vcenter_ip + " -e userName=" + vcenter_username + " -e password=" + vcenter_password,
            stderr=subprocess.STDOUT,
            shell=True)
    except subprocess.CalledProcessError:
            # If Ansible fails for any reason, stop the run.
            logger.error("\nError updating the mNode with the newest version of the VCP plug-in.  Please see the ansible.log file for details.\n")
            exit(2)
    logger.info("End - Updated the mNode with the newest version of the VCP Plugin.\n")


#  Main program execution #

# Create a ./ansible/hosts and a ./vars/vars_file.yml file using the vatrunner.properties file
try:
    build_hosts_file(args[vatrunner_properties])
except Exception.message:
    exit()

try:
    build_vars_file(args[vatrunner_properties])
except Exception.message:
    exit()

# If the user desires, take gold snapshots with memory
take_snapshots()
# Revert the VMs to their gold snapshot
revert_snapshots_to_gold()
# Reconnect the ESX servers to the vCenter (this is only a problem with 6.7, but do it anyway)
reconnect_esx_servers()
# Install the new VCP package on the mnode(s)
update_mnode()
