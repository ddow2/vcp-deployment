import logging
import logging.handlers


__LOG_NAME = 'vcp-deployment'
__LOG_FILE = 'vcp-deployment.log'
TRACE = 5

# Main logger class for logging all of the Python transactions.

# One-time setup of the root logger
def __setup_logger():

    logging.addLevelName(TRACE, 'TRACE')

    def trace(self, message, *args, **kws):
        """
        Log 'msg % args' with severity 'TRACE'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.
        """
        if self.isEnabledFor(TRACE):
            # Yes, logger takes its '*args' as 'args'.
            # pylint: disable=W0212
            self._log(TRACE, message, args, **kws)

    logging.Logger.trace = trace

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)

    log_formatter = logging.Formatter("%(levelname)s: %(asctime)s | %(module)15s.%(funcName)10s:%(lineno)-4d | %(message)s")

    file_handler = logging.handlers.WatchedFileHandler(__LOG_FILE)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(log_formatter)
    root_logger.addHandler(file_handler)

    # Setup the logger
    logger = logging.getLogger(__LOG_NAME)
    logger.setLevel(TRACE)

    return logger, file_handler


__LOGGER_INSTANCE, __FILE_HANDLER = __setup_logger()


def get_logger():
    """
    Return:
        The logger, creating it if it doesn't exist.
    """
    return __LOGGER_INSTANCE


def get_file_handler():
    """
    Return:
        The file handler, This is used by flask to log 500 errors.
    """
    return __FILE_HANDLER
